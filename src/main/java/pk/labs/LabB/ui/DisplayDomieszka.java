/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Display;

/**
 *
 * @author st
 */
@Component
public class DisplayDomieszka implements Negativeable
{
    Utils u;
    public DisplayDomieszka()
    {
        u = new Utils();
    }
    @Override
    public void negative() {
        u.negateComponent(((Display) AopContext.currentProxy()).getPanel());
    }
    
}
