/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.ControlPanel;

/**
 *
 * @author st
 */
@Component
public class ControlDomieszka implements Negativeable
{
    Utils u;
    public ControlDomieszka()
    {
        u = new Utils();
    }
    @Override
    public void negative() {
        u.negateComponent(((ControlPanel) AopContext.currentProxy()).getPanel());
    }
    
}
