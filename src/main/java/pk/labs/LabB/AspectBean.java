/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabB;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Logger;

/**
 *
 * @author Łukasz
 */
@Component
public class AspectBean {
    Logger logger;

    public Logger getLogger() {
        return logger;
    }
    public AspectBean()
    {
        this.logger = new LoggerImpl();
    }

    public void logBefore(JoinPoint jp){
        String s = jp.getSignature().getName();
        Object[] a = jp.getArgs();
        logger.logMethodEntrance(s,a);
    }
    public void logAfter(JoinPoint jp, Object obj){
        this.logger.logMethodExit(jp.getSignature().getName(), obj);
    } 
}

