package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.Contracts.impl.DisplayBean";
    public static String controlPanelImplClassName = "pk.labs.LabB.Contracts.impl.ControlBean";

    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.Main";
    public static String mainComponentImplClassName = "pk.labs.LabB.Contracts.impl.MainBean";
    public static String mainComponentBeanName = "mainBean";
    // endregion

    // region P2
    public static String mainComponentMethodName = "On";
    public static Object[] mainComponentMethodExampleParams = new Object[] {};
    // endregion

    // region P3
    public static String loggerAspectBeanName = "aspectBean";
    // endregion
}
